package poo2;


public class TesteHorario { 	
	public static void main(String[] args) {
		
		Horario h = new Horario();
		
		System.out.println(h);
		
		int i = 0;
		while(i < 70) {
			
			h.incrementaSegundo();
			h.incrementaMinuto();
			h.incrementaHora();
			
			System.out.println(h);
			
			i++;
		}
		
	}
}
